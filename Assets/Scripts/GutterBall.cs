﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GutterBall : MonoBehaviour {
    
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<BowlingBall>().InPlay == true && FindObjectOfType<PinSetter>().ballEnteredBox == false)
        {
            other.GetComponent<BowlingBall>().GutterBall();
        }
    }
}
