﻿using System.Collections.Generic;
using UnityEngine;

public static class ScoreMaster {

    public static List<int> ScoreCumulative (List<int> bowlRolls)
    {
        List<int> cumulativeScores = new List<int>();
        int runningTotal = 0;
        foreach(int frameScore in ScoreFrames(bowlRolls))
        {
            runningTotal += frameScore;
            cumulativeScores.Add(runningTotal);
        }
        return cumulativeScores;
    }

    public static List<int> ScoreFrames (List<int> rolls)
    {
        List<int> frameList = new List<int>();
        
            for (int i = 1; i < rolls.Count; i += 2)
            {
            if (frameList.Count == 10) { break; }
            if (rolls[i - 1] == 10)
            {
                if (rolls.Count > i + 1)
                {
                    frameList.Add(10 + rolls[i] + rolls[i + 1]);
                        if (i < 19)
                        {
                            i--;
                        }
                }
            }
            else if (rolls[i-1] + rolls[i] <10)
            {
                frameList.Add(rolls[i - 1] + rolls[i]);
            }
            else if (rolls[i - 1] + rolls[i] == 10)
            {
                if (rolls.Count > i + 1)
                {
                    frameList.Add(10 + rolls[i + 1]);
                }
            }

        }
        return frameList;
    }

}
