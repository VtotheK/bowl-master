﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    [SerializeField] Text standingDisplay;
    [SerializeField] ScoreDisplay scoreDisplay;
    private List<int> rolls = new List<int>();
    private List<int> cumulativeRolls = new List<int>();
    private void Awake()
    {
        ErrorLogger.ErrorLog(null, true);
    }



    public void BowlResult(int fallenPins)
    {
        try
        {
            rolls.Add(fallenPins);
            scoreDisplay.FillFrames(rolls);
            ActionMaster.Action action = ActionMaster.NextAction(rolls); //COMMENT Return the action for swiper, from ActionMaster
            SetSwiperAction(action);
            cumulativeRolls = ScoreMaster.ScoreCumulative(rolls);

            scoreDisplay.FillRolls(cumulativeRolls);
            
        }
        catch (System.IndexOutOfRangeException exception)
        {
            ErrorLogger.ErrorLog($"({exception} in GameManager, DisplayScore() broken", false); 
        }
        FindObjectOfType<PinSetter>().ballEnteredBox = false;
        FindObjectOfType<BowlingBall>().ResetBallPosition();
        standingDisplay.color = Color.green;
    }

    private static void SetSwiperAction(ActionMaster.Action action)
    {
        FindObjectOfType<Swiper>().SetSwiperAction(action);
    }
}
