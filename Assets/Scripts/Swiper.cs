﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swiper : MonoBehaviour {
    //ActionMaster actionMaster = new ActionMaster();
    Animator animator;
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void SetSwiperAction(ActionMaster.Action action)
    {
        if (action.Equals(ActionMaster.Action.Tidy))
        {
            animator.SetTrigger("tidyTrigger");
        }
        else if(action.Equals(ActionMaster.Action.Reset))
        {
            FindObjectOfType<PinSetter>().ResetLastSettledCount();
            animator.SetTrigger("resetTrigger");
        }
        else if (action.Equals(ActionMaster.Action.EndTurn))
        {
            FindObjectOfType<PinSetter>().ResetLastSettledCount();
            animator.SetTrigger("resetTrigger");
        }
        else if (action.Equals(ActionMaster.Action.EndGame))
        {
            throw new UnityException("Dont know how to handle EndGame!");
        }
    }

}
