﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinShredder : MonoBehaviour {
    [SerializeField] Text standingDisplay;

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<BowlingBall>())
        {
            standingDisplay.color = new Color(255f, 0f, 0f);
            FindObjectOfType<PinSetter>().ballEnteredBox = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        GameObject pin = other.gameObject;
        if (pin.GetComponentInParent<Pin>())
        {
            Destroy(pin.transform.parent.gameObject, 2);
        }
    }
}
