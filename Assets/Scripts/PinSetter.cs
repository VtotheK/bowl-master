﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinSetter : MonoBehaviour {
    [SerializeField] Text standingDisplay;
    [SerializeField] GameObject pins;
    BowlingBall bowlingBall;
    private int lastStandingCount = -1;

    private int lastSettledCount = 10;
    private float lastChangeTime;
    public bool ballEnteredBox = false;
	// Use this for initialization
	void Start ()
    {
        bowlingBall = FindObjectOfType<BowlingBall>();
        standingDisplay.text = ("10");
    }

    // Update is called once per frame
    void Update () {
        if (ballEnteredBox)
        {
            SetText(CountStanding());
            CheckStanding();
            foreach (Pin pin in FindObjectsOfType<Pin>())
            {
                pin.transform.position = new Vector3(1000, 1000, 2020);
            }
        }
    }

    void CheckStanding()
    {
        int currentStanding = CountStanding();

        if(currentStanding != lastStandingCount)
        {
            lastChangeTime = Time.time;
            lastStandingCount = currentStanding;
            return;
        }
        if(Time.time - lastChangeTime > 3f)
        {
            PinsHaveSettled();
        }
    }

    public void PinsHaveSettled()
    {
        int standing = CountStanding();
        int pinFall = lastSettledCount - standing;
        lastSettledCount = standing;
        FindObjectOfType<GameManager>().BowlResult(pinFall);
        
        lastStandingCount = -1;
    }

    private void SetText(int pins)
    {
        standingDisplay.text = pins.ToString();
    }

    public int CountStanding()
    {
        int standingPins = 0;
        foreach(Pin pin in GameObject.FindObjectsOfType<Pin>())
        {
            if(pin.IsStanding())
            {
                standingPins++;
            }
        }

        return standingPins;
    }


    public void Renew()
    {
        Instantiate(pins, new Vector3(0, 0, 1829), Quaternion.identity);
    }

    public void DisableMovement() { bowlingBall.InPlay = true; } //Called from Animator, Dont change function name
    public void EnableMovement() { bowlingBall.InPlay = false; }//Called from Animator, Dont change function name
    public void Raise() { foreach (Pin pin in FindObjectsOfType<Pin>()) { pin.RaiseIfStanding(); } }//Called from Animator, Dont change function name
    public void Lower(){ foreach (Pin pin in FindObjectsOfType<Pin>()) { pin.Lower(); } } //Called from Animator, Dont change function name
    public void ANIMATORresetStandingDisplay(){standingDisplay.text = "10"; } //Called from Animator, Dont change function name

    public void ResetLastSettledCount() { lastSettledCount = 10; } //REMEMBER Call this after Reset, EndTurn and EndGame ActionMaster actions
}
