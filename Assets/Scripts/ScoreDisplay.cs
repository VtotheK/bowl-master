﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class ScoreDisplay : MonoBehaviour {
    [SerializeField] Text[] frameText;
    [SerializeField] Text[] rollTexts;

    public void FillFrames(List<int> rolls)
    {
        foreach (int i in rolls)
        {
            Debug.Log(i + "ScoreDisplay");
        }
        string scoresString = FormatRolls(rolls);
        for (int i = 0; i < scoresString.Length; i++)
        {
            frameText[i].text = scoresString[i].ToString();
        }
    }

    public void FillRolls(List<int> frames)
    {
        for (int i = 0; i < frames.Count; i++)
        {
            rollTexts[i].text = frames[i].ToString();
        }
    }

    public static string FormatRolls(List<int> rolls)
    {
        StringBuilder output = new StringBuilder();
        int index = 0;
        int bowl = 0;
        foreach (int roll in rolls)
        {
            if(roll<0 || roll > 10) { throw new UnityException("Invalid roll count in ScoreDisplay.cs"); }
            if (roll == 0)
            {
                output.Append("-");
                bowl++;
                index++;
                continue;
            }
            if (bowl%2==0 && bowl < 18)
            {
                if(roll == 10)
                {
                    output.Append("X ");
                    index++;
                    bowl += 2;
                }
                else if(roll <10)
                {
                    output.Append(roll.ToString());
                    index++;
                    bowl++;
                }
            }
            else if (bowl%2!=0 && index < 19)
            {
                if(roll == 10)
                {
                    output.Append("/");
                    index++;
                    bowl++;
                }
                else if(roll <10)
                {
                    if (roll + rolls[index - 1] == 10)
                    {
                        output.Append("/");
                        index++;
                        bowl++;
                        continue;
                    }
                    else
                    {
                        output.Append(roll.ToString());
                        index++;
                        bowl++;
                    }
                }
            }
            else if(bowl>=18)
            {
                if(roll == 10) { output.Append("X");index++;bowl++; }
                else if (roll < 10)
                {
                    if (roll + rolls[index - 1] == 10 && bowl>=19)
                    {
                        output.Append("/");
                    }
                    else if (roll < 10)
                    {
                        output.Append(roll.ToString());
                    }
                    index++;
                    bowl++;
                }
            }
        }
        return output.ToString();
    }
}
