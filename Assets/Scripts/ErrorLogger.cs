﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public static class ErrorLogger  {

    public static void ErrorLog(string errorText, bool onAwake)
    {
        string filePath = @"c:\Testfolder\Errorlog\errorlog.txt"; //Path to create errorlog and check if log exists
        if (!onAwake)
        {
            if (!File.Exists(filePath) && Directory.Exists(@"c:\Testfolder\Errorlog\"))
            {
                using (StreamWriter writer = File.CreateText(filePath)) // Creating a text file to filePath location 
                {
                    DateTime time = DateTime.Now;
                    writer.WriteLine("Error log created at {0} \n.", time);
                }
            }
            else if (File.Exists(filePath))
            {
                using (StreamWriter writer = File.AppendText(filePath))
                {
                    DateTime time = DateTime.Now;
                    writer.WriteLine(errorText + " at " + time);
                }
            }
        }
        else if (onAwake)
        {
            Directory.CreateDirectory(@"c:\Testfolder\Errorlog\");
            File.CreateText(filePath);
        }
    }
}
