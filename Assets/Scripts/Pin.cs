﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pin : MonoBehaviour {
    // Use this for initialization

    public float standingThreshold = 5f;
    public float distanceToRaise = 50f;

    public bool IsStanding()
    {
        Vector3 rotationInEuler = transform.rotation.eulerAngles;
        float tiltInX = Mathf.Abs(rotationInEuler.x);
        float tiltInZ = Mathf.Abs(rotationInEuler.z);

        if(tiltInX <= standingThreshold && tiltInZ <= standingThreshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void RaiseIfStanding()
    {
        if (IsStanding())
        {
            MovePin(new Vector3(transform.position.x, distanceToRaise, transform.position.z));
            transform.rotation = Quaternion.Euler(0, 0, 0);
    }
    }

    public void Lower()
    {
        MovePin(new Vector3(transform.position.x, 0.5f, transform.position.z));
        GetComponent<Rigidbody>().useGravity = true;
    }

    private void MovePin(Vector3 direction)
    {
        GetComponent<Rigidbody>().useGravity = false;
        transform.position = direction;
    }
}
