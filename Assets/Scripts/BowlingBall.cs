﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowlingBall : MonoBehaviour {
    [SerializeField] Vector3 launchVelocity;
    private bool _InPlay = false;
    Vector3 ballStartPos;
    Rigidbody rigidBody;
    AudioSource audioSource;
    // Use this for initialization

    public bool InPlay
    {
        get { return _InPlay; }
        set { _InPlay = value; }
    }

    void Start ()
    {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.useGravity = false;

        ballStartPos = transform.position;

        //LaunchBall(launchVelocity);
    }

    public void LaunchBall(Vector3 velocity)
    {
        if (!InPlay)
        {
            rigidBody.useGravity = true;
            rigidBody.velocity = velocity + launchVelocity; //TODO Delete or edit this, dont need launchVelocity multiplier.

            audioSource = GetComponent<AudioSource>();
            audioSource.Play();

            InPlay = true;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        ClampXBeforeLaunch();
    }

    private void ClampXBeforeLaunch()
    {
        if (!InPlay)
        {
            float ballPosX = transform.position.x;
            Vector3 clampedPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            clampedPos.x = Mathf.Clamp(ballPosX, -50, 50);
            transform.position = clampedPos;
        }
    }

    public void ResetBallPosition()
    {
        ResetPosition();
        InPlay = false;
    }

    private void ResetPosition()
    {
        transform.position = ballStartPos;
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;
        rigidBody.rotation = Quaternion.identity;
        rigidBody.useGravity = false;
    }

    public void GutterBall()
    {
        ResetPosition();
        FindObjectOfType<PinSetter>().PinsHaveSettled();
    }
}
