﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
    [SerializeField] BowlingBall bowlingBall;
    Vector3 offset;
	// Use this for initialization
	void Start () {
        offset = transform.position - bowlingBall.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        MoveCamera();
    }

    private void MoveCamera()
    {
        if(bowlingBall.transform.position.z <= 1659f && bowlingBall.transform.position.y >=-50) 
        {
            transform.position = bowlingBall.transform.position + offset;
        }
    }
}
