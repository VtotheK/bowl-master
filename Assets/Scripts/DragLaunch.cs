﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BowlingBall))]
public class DragLaunch : MonoBehaviour {
    BowlingBall bowlingBall;

    Vector3 dragStart, dragEnd;
    float startTime, endTime;

	void Start () {
		bowlingBall = GetComponent<BowlingBall>();
	}

    public void DragStart() //Called from UI, dont change method name
    {
        dragStart = Input.mousePosition;
        startTime = Time.time; 
    }

    public void DragEnd() //Called from UI, dont change method name
    {
        endTime = Time.time;
        dragEnd = Input.mousePosition;

        float dragDuration = endTime - startTime;

        float lauchSpeedX = (dragEnd.x - dragStart.x) / dragDuration;
        float lauchSpeedZ = (dragEnd.y - dragStart.y) / dragDuration;

        Vector3 launchVelocity = new Vector3(lauchSpeedX, 0, lauchSpeedZ);

        bowlingBall.LaunchBall(launchVelocity);
    }


    public void MoveStart(float xNudge) //Called from UI, dont change method name
    {
        if (!bowlingBall.InPlay)
        {
            bowlingBall.transform.Translate(new Vector3(xNudge, 0, 0));
        }
    }

}
