﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using System.Linq;

[TestFixture]
public class ScoreTest
{

    ActionMaster.Action endTurn = ActionMaster.Action.EndTurn;
    ActionMaster.Action tidy = ActionMaster.Action.Tidy;
    ActionMaster.Action reset = ActionMaster.Action.Reset;
    ActionMaster.Action endGame = ActionMaster.Action.EndGame;
    List<int> bowls = new List<int>();

    [Test]
    public void T01StrikeReturnsEndTurn()
    {
        bowls.Add(10);
        Assert.AreEqual(endTurn, ActionMaster.NextAction(bowls));
    }

    [Test]
    public void T02SpareReturnsEndTurn()
    {
        bowls.Add(7);
        bowls.Add(3);
        Assert.AreEqual(endTurn, ActionMaster.NextAction(bowls));
    }
    [Test]
    public void T03Score3ReturnsTidy()
    {
        int[] rolls = { 3 };
        Assert.AreEqual(tidy, ActionMaster.NextAction(rolls.ToList()));
    }

    [Test]
    public void T04Score5And3ReturnsEndTurn()
    {
        int[] rolls = { 5 , 3 };
        Assert.AreEqual(endTurn, ActionMaster.NextAction(rolls.ToList()));

    }

    [Test]
    public void T05LastFrameBowl5ReturnTidy()
    {
        int[] rolls = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,};
        Assert.AreEqual(tidy, ActionMaster.NextAction(rolls.ToList()));
    }

    [Test]
    public void T06LastFrameBowl5And3ReturnsEndGame()
    {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 5, 3};
        Assert.AreEqual(endGame, ActionMaster.NextAction(rolls.ToList()));
    }

    [Test]
    public void T07LastFrameFirstBowlStrikeReturnsReset()
    {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10 };
        Assert.AreEqual(reset, ActionMaster.NextAction(rolls.ToList()));
    }

    [Test]
    public void T08LastFrameBowl21ReturnEndGame()
    {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1 };
        Assert.AreEqual(endGame, ActionMaster.NextAction(rolls.ToList()));
    }
    [Test]
    public void T09LastFrameBowl7And3SpareReturnReset()
    {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 7,3 };
        Assert.AreEqual(reset, ActionMaster.NextAction(rolls.ToList()));
    }

    [Test]
    public void T10LastFrameBowl7And3SpareAnd4ReturnEndGame()
    {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 7, 3,4 };
        Assert.AreEqual(endGame, ActionMaster.NextAction(rolls.ToList()));
    }

    [Test]
    public void T11LastFrameThreeStrikesReturnsEndGame()
    {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10,10,10 };
        Assert.AreEqual(endGame, ActionMaster.NextAction(rolls.ToList()));
    }

    [Test]
    public void T12StrikeBowl0Bowl7ReturnsEndGame()
    {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 0 , 7 };
        Assert.AreEqual(endGame, ActionMaster.NextAction(rolls.ToList()));
    }

    [Test]
    public void T13LastFrameBowl0And0ReturnEndGame()
    {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10,0,0 };
        Assert.AreEqual(endGame, ActionMaster.NextAction(rolls.ToList()));
    }
}